package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Lista;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	private GrafoNoDirigido<String, String> grafo;
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String


	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		grafo = new GrafoNoDirigido<String, String>();
		//TODO inicialice el grafo como un GrafoNoDirigido
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//		Lista<Arco<String, Estacion<String>>> temp = ;
		Arco<String, String>[] temp = grafo.darArcosDestino(identificador);
		String[] respuesta = new String[temp.length];
		for(int i = 0; i < temp.length; i++)
		{
			respuesta[i] = temp[i].darInformacion();
		}
		return respuesta;
		//TODO Implementar
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public Arco<String, String> distanciaMinimaEstaciones()
	{
		Arco<String, String>[] arcos= grafo.darArcos();
		Arco<String, String> respuesta = arcos[0];
		for(int i = 0; i < arcos.length; i++)
		{
			if(arcos[i].darCosto() < respuesta.darCosto())
			{
				respuesta = arcos[i];
			}
		}
		//TODO Implementar
		return respuesta;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public Arco<String, String> distanciaMaximaEstaciones()
	{
		
		Arco<String, String>[] arcos= grafo.darArcos();
		Arco<String, String> respuesta = arcos[0];
		for(int i = 0; i < arcos.length; i++)
		{
			if(arcos[i].darCosto() > respuesta.darCosto())
			{
				respuesta = arcos[i];
			}
		}
		//TODO Implementar
		return respuesta;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(new File(RUTA_PARADEROS)));
			int cantidad = Integer.parseInt(br.readLine());
			String entrada = br.readLine();
			while(entrada != null)
			{
				String[] datos = entrada.split(";");
				double latitud = Double.parseDouble(datos[1]);
				double longitud = Double.parseDouble(datos[2]);
				Estacion<String> temp = new Estacion<String>(datos[0], latitud, longitud);
				grafo.agregarNodo(temp);
				entrada = br.readLine();
				System.out.println(grafo.buscarNodo(datos[0]));
			}
			System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
			br.close();
		}
		catch (Exception e)
		{
			System.out.println("Error en la generacaión de los nodos: " + e.getMessage());
			e.printStackTrace();
		}

		//TODO Implementar
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(new File(RUTA_RUTAS)));
//			int cantidad = Integer.parseInt(br.readLine());
			String entrada = br.readLine();
			int contador = 0;

			String nombre = "";
			String cantidadEstaciones = "";
			String estacionAnterior = "";
//			System.out.println(entrada);
			entrada = br.readLine();
			while(entrada != null)
			{
//				System.out.println(entrada);
//				System.out.println(contador);
				if(entrada.equals("--------------------------------------"))
				{
					estacionAnterior = "";
					contador = 0;
//					System.out.println("1 IF");
				}
				else
				{
//					System.out.println("1 ELSE");
					if(contador == 0)
					{
						nombre = entrada;
//						System.out.println("2.1 if");
					}
					else if(contador == 1)
					{
						cantidadEstaciones = entrada;
//						System.out.println("2.2 if");
					}
					else
					{
//						System.out.println("2 else");
						String[] datos = entrada.split(" ");
						String estacionSiguiente = datos[0];
						double costo = Double.parseDouble(datos[1]);
						if(!estacionAnterior.equals(""))
						{
//							Estacion<String> anterior = (Estacion) grafo.buscarNodo(estacionAnterior);
//							Estacion<String> siguiente = (Estacion) grafo.buscarNodo(estacionSiguiente);
							grafo.agregarArco(estacionAnterior, estacionSiguiente, costo, nombre);
//							System.out.println(estacionAnterior + ": " + grafo.darArcosOrigen(estacionAnterior) + ", " + grafo.darArcosOrigen(estacionAnterior).length);
						}
//						Arco<String, Estacion<String>> temp = new Arco<String, Estacion<String>>(inicio, fin, costo);
						
						estacionAnterior = datos[0];
					}
					contador++;
				}
				
//				double latitud = Double.parseDouble(datos[1]);
//				double longitud = Double.parseDouble(datos[2]);
//				Estacion<String> temp = new Estacion<String>(datos[0], latitud, longitud);
//				grafo.agregarNodo(temp);
				
				entrada = br.readLine();
				
//				System.out.println(grafo.buscarNodo(datos[0]));
			}
			br.close();
//			System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		return (Estacion<String>) grafo.buscarNodo(identificador);
	}

}
