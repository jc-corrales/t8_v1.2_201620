package estructuras;

public class Lista <T>
{
	// -------------------------------------------------------------
	// Constantes
	// -------------------------------------------------------------
	private final static int CAPACIDADMAXDEFAULT = 1024;
	/**
	 * La liste que contiene todo.
	 */
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------
	private T[] array;
	/**
	 * Atributo que contiene el tamaño actual de la lista.
	 */
	private int size;
	/**
	 * Atributo que contiene el tamaño máximo actual.
	 */
	private int tamMaxActual;
	// -------------------------------------------------------------
	// Constructores
	// -------------------------------------------------------------
	/**
	 * Constructor de la Lista
	 */
	@SuppressWarnings("unchecked")
	public Lista()
	{
		array = (T[]) new Object[CAPACIDADMAXDEFAULT];
		size = 0;
		tamMaxActual = CAPACIDADMAXDEFAULT;
	}
	
	// -------------------------------------------------------------
	// Métodos
	// -------------------------------------------------------------
	/**
	 * Método que retorna el tamaño de la lista.
	 * @return
	 */
	public int size()
	{
		return size;
	}
	/**
	 * Método que agrega un elemento a la lista.
	 * @param elemento
	 */
	public void add(T elemento)
	{
		if(size >= array.length)
		{
			resize();
		}
		array[size] = elemento;
		size++;
	}

	/**
	 * Método que elimina un elemento en la posición dada.
	 * @param pos
	 * @return T elemento eliminado.
	 * @throws IndexOutOfBoundsException
	 */
	@SuppressWarnings("unchecked")
	public T remove(int pos)throws IndexOutOfBoundsException
	{
		if(pos < size)
		{
			T elemento = null;
			T[]temp = (T[]) new Object[size];
			int j = 0;
			for(int i = pos+1; i < size && array[i] != null; i++)
			{
				temp[j] = array[i];
				j++;
			}
			elemento = array[pos];
			array[pos] = null;
			int l = 0;
			for(int k = pos; k < size; k++)
			{
				array[k] = temp[l];
				l++;
			}
			size--;
			return elemento;
		}
		else
		{
			throw new IndexOutOfBoundsException("Se está buscando un elemento fuera del tamaño de la lista.");
		}
	}
	/**
	 * Método que elimina el último elemento.
	 * @return T elemento eliminado.
	 */
	public T delete()
	{
		T elemento = array[size-1];
		array[size - 1] = null;
		size--;
		return elemento;
	}

	/**
	 * Método que reemplaza un elemento en una posición dada con un nuevo elemento.
	 * @param elemento
	 * @param pos
	 * @throws IndexOutOfBoundsException
	 */
	public void set(T elemento, int pos)throws IndexOutOfBoundsException
	{
		if(pos < size)
		{
			array[pos] = elemento;
		}
		else
		{
			throw new IndexOutOfBoundsException("Se está buscando un elemento fuera del tamaño de la lista.");
		}
	}

	/**
	 * Método que limpia toda la lista, al crea una nueva reemplazando la antigua.
	 */
	@SuppressWarnings("unchecked")
	public void clear()
	{
		array = (T[]) new Object[CAPACIDADMAXDEFAULT];
		size = 0;
	}

	/**
	 * Método que busca un elemento y retorna true si lo encuentra, false de lo contrario.
	 * @param criterio elemento a buscar.
	 * @return boolean si se encuentra el elemento referenciado o no.
	 */
	public boolean contains(T criterio)
	{
		boolean encontro = false;
		for(int i = 0; i < size; i++)
		{
			if(array[i].equals(criterio))
			{
				encontro = true;
				break;
			}
		}
		return encontro;
	}
	
	/**
	 * Método que retorna el contenido de una posición.
	 * @param pos Posición del objeto buscado.
	 * @return Objeto en la posición dada.
	 * @throws IndexOutOfBoundsException
	 */
	public T get(int pos)throws IndexOutOfBoundsException
	{
		if(pos < size)
		{
			return array[pos];
		}
		else
		{
			throw new IndexOutOfBoundsException("Se está buscando un elemento fuera del tamaño de la lista.");
		}
	}
	/**
	 * Método que indica si la lista está vacía o no.
	 * @return boolean Si la lista está vacía retorna true, false de lo contrario.
	 */
	public boolean isEmpty()
	{
		return (size == 0);
	}
	/**
	 * Método que retorna todo el contenido de la lista en un arreglo.
	 * @return Array arreglo completo de la lista.
	 */
//	@SuppressWarnings("unchecked")
	public T[] toArray()
	{
		T[] temp = (T[]) new Object[size];
		for(int i = 0; i < size; i++)
		{
			temp[i] = array[i];
		}	
		return temp;
	}
	/**
	 * Método que busca la posición de un objeto dado, retorna -1 si no existe.
	 * @param objeto
	 * @return
	 */
	public int getPos(T objeto)
	{
		int pos = -1;
		for(int i = 0; i < size; i++)
		{
			if(array[i].equals(objeto))
			{
				pos = i;
			}
		}
		return pos;
	}
	/**
	 * Método que elimina un objeto que entra por parámetro.
	 * @param elemento
	 */
	public T delete(T elemento)
	{
		int pos = getPos(elemento);
		return remove(pos);
	}
	
	/**
	 * Método privado que amplia el tamaño de la lista, duplicando su tamaño.
	 */
	@SuppressWarnings("unchecked")
	private void resize()
	{
		int tamNuevo = (tamMaxActual*2);
		T[]temp = (T[]) new Object[tamNuevo];
		for(int i = 0; i < size; i++)
		{
			temp[i] = array[i];
		}
		array = (T[]) new Object[tamNuevo];
		for(int j = 0; j < size; j++)
		{
			array[j] = temp[j];
		}
		tamMaxActual = tamNuevo;
	}
}
