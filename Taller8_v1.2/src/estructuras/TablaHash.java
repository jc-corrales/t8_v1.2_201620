package estructuras;

import java.lang.reflect.Array;


/**
 * 
 * @author JuanCarlos
 *
 * @param <K> Clase de las llaves.
 * @param <V> Clase de los valores.
 */
public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private Lista<NodoHash<K, V>>[] array;
	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;
	/**
	 * Atributo que contiene las llaves
	 */
	private Lista<K> llaves;
	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		array = new Lista[1024];
		capacidad = 1024;
		for(int i = 0; i < capacidad; i++)
		{
			array[i] = new Lista<NodoHash<K, V>>();
		}
		count = 0;
		factorCarga = 0;
		factorCargaMax = ((75*capacidad)/100);
		llaves = new Lista<K>();
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
	}
	

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax)
	{
		array = new Lista[capacidad];
		count = 0;
		for(int i = 0; i < capacidad; i++)
		{
			array[i] = new Lista<NodoHash<K, V>>();
		}
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		llaves = new Lista<K>();
		//TODO: Inicialice la tabla con los valores dados por parametro

	}

	public void put(K llave, V valor)
	{
//		System.out.println("Agregando: " + valor + " Llave: " + llave);
		int pos = hash(llave);
		NodoHash<K, V> nuevo = new NodoHash<K, V>(llave, valor);

		V criterio = get(llave);
		if(criterio == null)
		{
			array[pos].add(nuevo);
			llaves.add(llave);
			if(array[pos].size() == 1)
			{
				count++;
			}
			
				
			reHash();	
		}
		else
		{
			NodoHash<K, V> nodo = null;
			int pos2 = hash(llave);
			System.out.println("Colisión, llave: " + llave);
			for(int i = 0; i < array[pos2].size(); i++)
			{
				if(array[pos2].get(i).getLlave().equals(llave))
				{
					nodo = array[pos2].get(i);
				}
			}
			nodo.setValor(valor);
		}
		
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
	}
	/**
	 * Método que obtiene el valor asociado a una llave dada.
	 * @param llave llave del valor a buscar.
	 * @return Valor del elemento buscado.
	 */
	public V get(K llave)
	{
		V elemento = null;
		NodoHash<K, V> nodo = null;
		int pos = hash(llave);
		for(int i = 0; i < array[pos].size(); i++)
		{
			if(array[pos].get(i).getLlave().equals(llave))
			{
				nodo = array[pos].get(i);
			}
		}
		
		if(nodo != null)
		{
			elemento = nodo.getValor();
		}
		
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		return elemento;
	}
	
	/**
	 * Método que elimina el valor buscado y retorna el elemento.
	 * @param llave
	 * @return V elemento.
	 */
	public V delete(K llave)
	{
		V elemento = null;
		NodoHash<K, V> nodo = null;
		int pos = hash(llave);
		for(int i = 0; i < array[pos].size(); i++)
		{
			if(array[pos].get(i).getLlave().equals(llave))
			{
				nodo = array[pos].get(i);
				array[pos].remove(i);
				llaves.delete(llave);
			}
		}
		elemento = nodo.getValor();
		if(array[pos].size() == 0)
		{
			count--;
		}
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		return elemento;
	}
	
	/**
	 * Método que retorna la cantidad de objetos actuales.
	 * @return
	 */
	public int size()
	{
		return count;
	}
	
//	public V[] toArrayValores()
//	{
////		V[] respuesta = (V[])new Object[llaves.size()];
//		Lista<V> answer = new Lista<V>();
////		V[] respuesta = (V[]) Array.newInstance(V, llaves.size());
////		System.out.println(llaves.getClass());
////		System.out.println(respuesta.getClass());
//		for(int i = 0; i < llaves.size(); i++)
//		{
//			System.out.println("Elemento en Tabla Hash: " + get(llaves.get(i)).getClass());
////			respuesta[i] = get(llaves.get(i));
//			answer.add(get(llaves.get(i)));
////			System.out.println("Elemento en arreglo: " + respuesta[i].getClass());
//			System.out.println("Elemento en arreglo: " + answer.get(i).getClass());
//		}
////		System.out.println(respuesta.getClass());
//		System.out.println(answer.getClass());
//		System.out.println(answer.toArray().getClass());;
//		V[] respuesta = (V[])answer.toArray();
//		System.out.println(respuesta.getClass());
//		return respuesta;
//	}
	
	public Object[] darLlaves()
	{
		return (Object[])llaves.toArray();
	}
	//Hash
	/**
	 * Método que retorna la posición de una tupla usando su llave.
	 * @param llave de la tupla.
	 * @return posición en la tabla de hash.
	 */
	private int hash(K llave)
	{
		int pos = llave.hashCode()%capacidad;
		if(pos < 0)
		{
			pos = Math.abs(pos);
		}
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return pos;
	}
	
	private void calcularFactorDeCargaActual()
	{
		factorCarga = (count*100)/capacidad;
//		System.out.println(factorCarga);
	}
	/**
	 * Método que verifica si el rehash es necesario, y de serlo, lo realiza.
	 */
	@SuppressWarnings("unchecked")
	private void reHash()
	{
		calcularFactorDeCargaActual();
		if(factorCarga >= factorCargaMax)
		{
			Lista<NodoHash<K, V>>[] original = new Lista[capacidad];
			original = array;
			int capacidadVieja = capacidad;
			capacidad = (capacidad*2);
			array = new Lista[capacidad];

			for(int i = 0; i < capacidad; i++)
			{
				array[i] = new Lista<NodoHash<K, V>>();
			}

			for(int i = 0; i < capacidadVieja; i++)
			{
				while(!original[i].isEmpty())
				{
					NodoHash<K, V> elemento = original[i].delete();
					int pos = hash(elemento.getLlave());
					array[pos].add(elemento);
				}
			}
		}		
//		for(int l = 0; l < array.length; l++)
//		{
//			System.out.println(array[l].size());
//		}
	}
		//TODO: Permita que la tabla sea dinamica

}