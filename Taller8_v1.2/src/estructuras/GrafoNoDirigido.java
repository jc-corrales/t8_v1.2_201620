package estructuras;

import modelos.Estacion;


/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E>
{
	//TODO NOTAS:
	//

	/**
	 * Nodos del grafo
	 */
	private TablaHash<K, Nodo<K>> nodos;
	//TODO Declare la estructura que va a contener los nodos

	/**
	 * Lista de adyacencia 
	 */
	private TablaHash<K, Lista<Arco<K,E>>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido()
	{
		nodos = new TablaHash<K, Nodo<K>>();
		adj = new TablaHash<K, Lista<Arco<K,E>>>();
		//TODO implementar
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo)
	{
		try
		{
			nodos.put(nodo.darId(), nodo);
			return true;
		}
		catch(Exception e)
		{
			return false;
		}		
		//TODO implementar
	}

	@Override
	public boolean eliminarNodo(K id)
	{
		try
		{
			nodos.delete(id);
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
		//TODO implementar
	}

	@Override
	public Arco<K,E>[] darArcos()
	{
		//		Arco[] array= new Arco[adj.size()];
		Object[] llaves = adj.darLlaves();
		Lista<Arco<K,E>> listaElementos = new Lista<Arco<K,E>>();
		for(int i = 0; i < llaves.length; i++)
		{
			K llave = (K)llaves[i];
			Lista<Arco<K,E>> listaTemp = adj.get(llave);
			for(int j = 0; j < listaTemp.size(); j++)
			{
				Arco<K,E> elemento = listaTemp.get(j);
				listaElementos.add(elemento);
			}
			//			Lista<Arco<K,E>> listaTemp = 
			//			while()
			//			{
			//				elementos[i] = adj.get(temp);
			//			}
		}
		Arco<K,E>[] elementos = (Arco<K,E>[]) new Arco[listaElementos.size()];
		for(int i = 0; i < listaElementos.size(); i++)
		{
			elementos[i] = listaElementos.get(i);
		}

		//		Lista<Arco<K,E>>[] tempLista = adj.toArrayValores();
		//		Lista<Arco<K,E>> respuesta = new Lista<Arco<K,E>>();
		//		for(int i = 0; i < tempLista.length; i++)
		//		{
		//			for(int j = 0; j < tempLista[i].size(); j++)
		//			{
		//				respuesta.add(tempLista[i].get(j));
		//			}
		//		}
		return elementos;
		//TODO implementar
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		//		return new Arco<K, E>(buscarNodo(inicio), buscarNodo(fin), costo, e);
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos()
	{
		//		System.out.println(nodos.toArrayValores().getClass());
		//		Lista<Nodo<K>> nodosTemp = new Lista<Nodo<K>>();
		//		System.out.println("Antes dar llaves");
		Object[] llaves = nodos.darLlaves();
		//		System.out.println("Despues dar llaves & antes de elementos");
		Nodo<K>[] elementos = (Nodo<K>[]) new Nodo[llaves.length];
		for(int i = 0; i < llaves.length; i++)
		{
			K temp = (K)llaves[i];
			//			System.out.println("Llave = " + temp);
			//			System.out.println(nodos.get(temp).getClass());
			//			nodosTemp.add(nodos.get(temp));
			elementos[i] = nodos.get(temp);
			//			System.out.println(temp);
			//			System.out.println(nodos.get(temp));
		}
		//		System.out.println(nodosTemp.toArray().getClass());
		//		Nodo<K>[] respuesta = (Nodo<K>[])nodosTemp.toArray();
		return elementos;
		//		return respuesta;
		//TODO implementar
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj)
	{
		try
		{
			if((crearArco(inicio, fin, costo, obj) != null))
			{
				K llave = inicio;
				Arco<K,E> temp = crearArco(inicio, fin, costo, obj);
				if(adj.get(llave) != null)
				{
					Lista<Arco<K,E>> temp2 = adj.get(llave);
					temp2.add(temp);
				}
				else
				{
					Lista<Arco<K,E>> temp2 = new Lista<Arco<K,E>>();
					temp2.add(temp);
					adj.put(llave, temp2);
				}
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception e)
		{
			return false;
		}
		//TODO implementar
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo)
	{
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin)
	{
		Arco<K, E> respuesta = null;
		try
		{

			if((nodos.get(inicio) != null) && (nodos.get(fin) != null))
			{
				Lista<Arco<K, E>> lista = adj.get(inicio);
				for(int i = 0; i < lista.size(); i++)
				{
					if(lista.get(i).darNodoFin().equals(fin))
					{
						respuesta = lista.remove(i);
						break;
					}
				}
			}
			return respuesta;
		}
		catch(Exception e)
		{
			System.out.println("Error en la eliminación de un Arco: " + e.getMessage());
			return respuesta;
		}
	}

	@Override
	public Nodo<K> buscarNodo(K id)
	{
		//TODO implementar
		return nodos.get(id);
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id)
	{
		Lista<Arco<K,E>> temp = adj.get(id);
		Arco<K,E>[] respuesta = (Arco<K,E>[])new Arco[temp.size()];


		for(int i = 0; i < temp.size(); i++)
		{
			Arco<K,E> nodoTemp = temp.get(i);
			//			System.out.println("Llave = " + temp);
			//			System.out.println(nodos.get(temp).getClass());
			//			nodosTemp.add(nodos.get(temp));
			respuesta[i] = nodoTemp;
			//			System.out.println(temp);
			//			System.out.println(nodos.get(temp));
		}
		//TODO implementar
		//		return adj.get(id).toArray();
		return respuesta;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id)
	{
		Lista<Arco<K,E>> listaDeNodosDestino = adj.get(id);
		
		//TODO implementar
		//		Lista<Arco<K,E>>[] tempLista = adj.toArrayValores();
		//		Lista<Arco<K,E>> respuesta = new Lista<Arco<K,E>>();
		Nodo<K>[] llaves = darNodos();
		
		Lista<Arco<K,E>> listaDeRespuesta = new Lista<Arco<K,E>>();
		
		for(int i = 0; i < llaves.length; i++)
		{
			Lista<Arco<K, E>> listaPos = adj.get(llaves[i].darId());
			if(listaPos != null)
			{
				for(int j = 0; j < listaPos.size(); j++)
				{
					if(listaPos.get(j).darNodoFin().darId().equals(id))
					{
						listaDeRespuesta.add(listaPos.get(j));
					}
				}
			}
		}
		
		Arco<K,E>[] respuesta = (Arco<K,E>[])new Arco[listaDeRespuesta.size()];
		
		for(int i = 0; i < listaDeRespuesta.size(); i++)
		{
			K temp = (K)llaves[i];
			respuesta[i] = listaDeNodosDestino.get(i);
		}
		return respuesta;
	}

}
